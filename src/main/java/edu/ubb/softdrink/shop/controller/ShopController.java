package edu.ubb.softdrink.shop.controller;
import edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.model.Order;
import edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.model.Payment;
import edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class ShopController {
    @Autowired
    ShopService service;

    @GetMapping(path="api/v1/shop/listorders")
    public String getAllSoftDrinksOrders() {
        return "Hardcoded list of SoftDrinks orders: Order 1: 10 0.5L Cola, 12 2L Fanta, 10 2L Sprite, Order2: 10 1L Cola, 20 0.5L Fanta Grapefruit";
    }

    @GetMapping(path="api/v2/shop/listorders")
    public String getAllSoftDrinksOrdersV2() {
        return service.returnAllOrders();
    }

    @PostMapping(path="api/v1/shop/add")
    @ResponseBody
    public String orderSoftDrinkV1(@RequestBody String orderItems) {
        StringBuilder sb = new StringBuilder();
        sb.append("Your order is: \n");
        sb.append(orderItems);
        sb.append("Status: PENDING! \nPlease pay your order");

        return sb.toString();
    }

    @PostMapping(path="api/v2/shop/add")
    @ResponseBody
    public String orderSoftDrinkV2(@RequestBody Order order) {
        return service.saveData(order);
    }

    @PostMapping(path="api/v1/shop/pay")
    @ResponseBody
    public String payOrderV1(@RequestBody String paymentMethod) {
        return "Purchase successful!\nPayment method: " + paymentMethod;
    }

    @PostMapping(path="api/v2/shop/pay")
    @ResponseBody
    public String payOrderV2(@RequestBody Payment paymentInfo) {
        return service.updateOrderStatus(paymentInfo);
    }

}
