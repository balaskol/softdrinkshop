package edu.ubb.softdrink.shop.repository;

import edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.model.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Integer> {

}
