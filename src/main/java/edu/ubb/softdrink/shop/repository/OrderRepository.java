package edu.ubb.softdrink.shop.repository;

import edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {

}
