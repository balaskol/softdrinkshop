package edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.service;

import edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.dto.StockRequestDto;
import edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.model.Order;
import edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.model.OrderItem;
import edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.model.Payment;
import edu.ubb.softdrink.shop.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ShopService {
    @Autowired
    OrderRepository orderRepository;

    private static final String STOCK_REST_URI = "http://softdrink-stock-service:8080/api/stocks?version=1";

    public String saveData(Order order){
        List<StockRequestDto> stockItemData = new ArrayList<>();
        for (OrderItem orderItem : order.getItemsList()) {
            stockItemData.add(new StockRequestDto(orderItem.getDrinkId(), orderItem.getDrinkQuantity()));
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<List<StockRequestDto>> request = new HttpEntity<List<StockRequestDto>>(stockItemData);
        ResponseEntity<String> response = restTemplate
                .exchange(STOCK_REST_URI, HttpMethod.POST, request, String.class);
        float totalPrice = Float.parseFloat(response.getBody());
        StringBuilder sb = new StringBuilder();

        if(totalPrice == 0){
            sb.append("Order failed: sorry, one or more items from your order went out of stock");
        }else {
            sb.append("Your order is: \n");
            order.setDate(LocalDateTime.now());
            order.setTotal((float) 15.0);
            sb.append(order.toString());
            sb.append("Items:\n");
            for (OrderItem orderItem : order.getItemsList()) {
                orderItem.setOrder(order);
                sb.append(orderItem.toString());
            }

            order.setStatus("PENDING");
            orderRepository.save(order);
            sb.append("Status: PENDING! \nPlease pay your order");
        }

        return sb.toString();
    }

    public String returnAllOrders(){
        StringBuilder sb = new StringBuilder();
        sb.append("List of all orders:\n");
        Iterable<Order> orders = orderRepository.findAll();
        for(Order order : orders){
            sb.append("Order id:" + order.getId() + "\n");
            sb.append(order.toString());
            sb.append("\nItems:\n");
            for(OrderItem orderItem : order.getItemsList()){
                sb.append(orderItem.toString());
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    public String updateOrderStatus(Payment paymentInfo){
        Order order = orderRepository.findById(paymentInfo.getOrderId()).get();
        order.setStatus("PAID");
        orderRepository.save(order);

        return "Order status updated: PAID\n" + paymentInfo.toString();
    }
}
