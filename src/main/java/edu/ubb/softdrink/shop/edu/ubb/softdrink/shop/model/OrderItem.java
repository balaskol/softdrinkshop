package edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.model;

import javax.persistence.*;

@Entity
@Table(name="order_items")
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="order_item_id")
    private int orderItemId;
    @Column(name="drink_id")
    private int drinkId;
    @Column(name="drink_name")
    private String drinkName;
    @Column(name="drink_quantity")
    private int drinkQuantity;
    @Column(name="drink_size")
    private float drinkSize;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    public int getOrderItemIdId() {
        return orderItemId;
    }

    public void setOrderItemIdId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public int getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(int drinkId) {
        this.drinkId = drinkId;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public void setDrinkName(String drinkName) {
        this.drinkName = drinkName;
    }

    public int getDrinkQuantity() {
        return drinkQuantity;
    }

    public void setDrinkQuantity(int drinkQuantity) {
        this.drinkQuantity = drinkQuantity;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public float getDrinkSize() {
        return drinkSize;
    }

    public void setDrinkSize(float drinkSize) {
        this.drinkSize = drinkSize;
    }

    public void setSize(float drinkSize) {
        this.drinkSize = drinkSize;
    }

    @Override
    public String toString() {
        return "Name:" + drinkName + ", quantity:" + drinkQuantity + ", size:" + drinkSize +"L\n";
    }
}
