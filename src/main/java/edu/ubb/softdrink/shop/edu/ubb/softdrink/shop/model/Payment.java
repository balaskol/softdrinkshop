package edu.ubb.softdrink.shop.edu.ubb.softdrink.shop.model;

public class Payment {
    private String paymentMethod;
    private int orderId;

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "Payment method:" + paymentMethod + ", orderId:" + orderId;
    }
}
