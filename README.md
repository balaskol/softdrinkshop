# Project description
## SoftDrinksShop
* This project is an example project for university on Microservices.
* In this project we order softdrinks, we can list the orders and pay them. The implementation is not a production implementation.

## Components:
* Spring server
* MySQL server 

The MySQL server connection is set to connect to Kubernetes.
MySQL database, tables creation script can be found in mysql_scripts.txt .

## Endpoints:
* The v1 version endpoints are hardcoded, the v2 communicates with database.

1. api/v1/shop/listorders  - list our orders  - GET request
 parameters: no parameters

2. api/v1/shop/add  - create an order - POST request
 parameters: string with ordered items

3. api/v1/shop/pay - pay order  - POST request
 parameters: a string with name of payment method

4. api/v2/shop/listorders - GET request
 parameters: no parameters

5. api/v2/shop/add - POST request
 parameters: array of Order objects.
 This endpoint communicates with Erik Miklos's project which can be found at: https://bitbucket.org/mikloserik/softdrinkstock/src/master/

6. api/v2/shop/pay - POST request
 parameters: json data mapped by Payment object.
